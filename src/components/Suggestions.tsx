import * as React from "react";
import {MainStore} from "../store";
import {Suggestion} from "../store/suggestions";
import {connect} from "react-redux";
import {Grid} from "@material-ui/core";


interface Props {
    suggestions: Suggestion[];
    queryTime: number | undefined;
}


class Suggestions extends React.Component<Props>{

    public static renderSuggestion(suggestion: Suggestion): React.ReactNode{
        return(
            <React.Fragment>
                <Grid item xs={12}>
                    {suggestion.name}
                </Grid>
                <Grid container>
                    <Grid item xs={2}>
                        <label style={{color: "grey", fontStyle: "italic", fontSize: '12px'}}>Score: {suggestion.score} </label>
                    </Grid>
                    <Grid item xs={2}>
                        <label style={{color: "grey", fontStyle: "italic", fontSize: '12px'}}>Lat: {suggestion.latitude} </label>
                    </Grid>
                    <Grid item xs={2}>
                        <label style={{color: "grey", fontStyle: "italic", fontSize: '12px'}}>Lon: {suggestion.longitude} </label>
                    </Grid>
                    {suggestion.distance && <Grid item xs={4}>
                        <label style={{color: "grey", fontStyle: "italic", fontSize: '12px'}}>Distance: {suggestion.distance.km}km  {suggestion.distance.mi}miles  </label>
                    </Grid>}
                </Grid>
            </React.Fragment>
        );
    }

    public render(): React.ReactNode {
        const {suggestions, queryTime} = this.props;
        return (
            <React.Fragment>
                {queryTime && <label style={{color: "grey", fontStyle: "italic", fontSize: '12px'}}>query took {queryTime}ms to execute </label>}
                {suggestions.map((x: Suggestion) => {
                    return(
                        <React.Fragment>
                            <Grid item xs={12}>
                                {Suggestions.renderSuggestion(x)}
                            </Grid>
                        </React.Fragment>
                    )
                })}
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state: MainStore) => {
    return {
        suggestions: state.suggestionsStore.suggestions,
        queryTime: state.suggestionsStore.queryTime
    };
};

export default connect(mapStateToProps, undefined)(Suggestions);