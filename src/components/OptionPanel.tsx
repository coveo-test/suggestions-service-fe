import * as React from "react";
import {MainStore} from "../store";
import {updateExtended, updateLatitude, updateLongitude, updateUseCoords} from "../store/suggestions";
import {connect} from "react-redux";
import {
    Checkbox,
    ExpansionPanel,
    ExpansionPanelDetails,
    ExpansionPanelSummary, FormControlLabel,
    TextField,
    Typography
} from "@material-ui/core";
import {ExpandMore} from "@material-ui/icons";

interface Props {
    latitude: number;
    longitude: number;
    useCoords: boolean;
    extended: boolean;
    updateLatitude: Function;
    updateLongitude: Function;
    updateUseCoords: Function;
    updateExtended: Function;
}

class OptionPanel extends React.Component<Props> {
    public render(): React.ReactNode {

        const {latitude, longitude, useCoords, extended, updateLatitude, updateLongitude, updateUseCoords, updateExtended} = this.props;
        return (
            <React.Fragment>
                <ExpansionPanel>
                    <ExpansionPanelSummary
                        expandIcon={<ExpandMore />}
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                    >
                        <Typography>Options</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <TextField
                            id="standard-number"
                            label="Latitude"
                            value={latitude || 0}
                            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {updateLatitude(event.target.value)}}
                            type="number"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            margin="normal"
                        />
                        <TextField
                            id="standard-number"
                            label="Longitude"
                            value={longitude || 0}
                            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {updateLongitude(event.target.value)}}
                            type="number"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            margin="normal"
                        />
                        <FormControlLabel
                            control={

                                <Checkbox
                                    checked={useCoords}
                                    onChange={(event: React.ChangeEvent<HTMLInputElement>) => {updateUseCoords(event.target.checked)}}
                                    value={"Use coordinates"}
                                    inputProps={{
                                        'aria-label': 'primary checkbox',
                                    }}
                                />
                            }
                            label="Use coordinates"
                        />
                        <FormControlLabel
                            control={

                                <Checkbox
                                    checked={extended}
                                    onChange={(event: React.ChangeEvent<HTMLInputElement>) => {updateExtended(event.target.checked)}}
                                    value={"Extended results"}
                                    inputProps={{
                                        'aria-label': 'primary checkbox',
                                    }}
                                />
                            }
                            label="Extended results"
                        />
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state: MainStore) => {
    return {
        latitude: state.suggestionsStore.latitude,
        longitude: state.suggestionsStore.longitude,
        useCoords: state.suggestionsStore.useCoords,
        extended: state.suggestionsStore.extended
    };
};

const mapDispatchToProps = {
    updateLatitude: updateLatitude,
    updateLongitude: updateLongitude,
    updateUseCoords: updateUseCoords,
    updateExtended: updateExtended,
};

export default connect(mapStateToProps, mapDispatchToProps)(OptionPanel);