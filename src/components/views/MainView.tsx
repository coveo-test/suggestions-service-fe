import {MainStore} from "../../store";
import {updateQuery} from "../../store/suggestions";
import {connect} from "react-redux";

import * as React from "react";
import Suggestions from "../Suggestions";
import {
    Grid,
    TextField
} from "@material-ui/core";

import {ChangeEvent} from "react";
import OptionPanel from "../OptionPanel";


interface Props {
    query: string;
    updateQuery: Function;
}

class MainView extends React.Component<Props>{
    public render(): React.ReactNode {

        const {query, updateQuery} = this.props;
        return (
            <React.Fragment>
                <Grid container>
                    <Grid item xs={12}>
                        <OptionPanel />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField label={"query"} value={query} onChange={(e: ChangeEvent<HTMLInputElement>)=>updateQuery(e.target.value)} />
                    </Grid>
                    <Grid item xs={12}>
                        <Suggestions/>
                    </Grid>
                </Grid>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state: MainStore) => {
    return {
        query: state.suggestionsStore.query
    };
};

const mapDispatchToProps = {
    updateQuery: updateQuery,
};

export default connect(mapStateToProps, mapDispatchToProps)(MainView);