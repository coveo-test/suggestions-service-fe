import React, { Component } from 'react';
import {applyMiddleware, createStore} from "redux";
import {rootReducer} from "./store";
import {Provider} from "react-redux";
import TestView from "./components/views/MainView";
import thunk from "redux-thunk";
import {asyncDispatchMiddleware} from "./store/middleware";

class App extends Component {

    public static configureStore(){
        const initialStore = {};
        const middlewares = [];
        if (process.env.NODE_ENV === "development") {
            const { createLogger } = require("redux-logger");
            middlewares.push(createLogger());
        }
        middlewares.push(thunk);
        middlewares.push(asyncDispatchMiddleware);
        return createStore(rootReducer, initialStore, applyMiddleware(...middlewares))
    }

    public render() {
        const store = App.configureStore();
        return (
            <Provider store={store}>
                <TestView />
            </Provider>
        );
    }
}

export default App;
