import {action} from "typesafe-actions";
import {SuggestionsActionTypes, SuggestionsStore} from "./types";

export const fetchSuggestionsResponse = (query: string, response: string) => action(SuggestionsActionTypes.FETCH_RESPONSE, {'query': query, 'response': response});
export const updateQuery = (query: string) => action(SuggestionsActionTypes.UPDATE_QUERY, query);
export const updateLatitude = (latitude: number) => action(SuggestionsActionTypes.UPDATE_LATITUDE, latitude);
export const updateLongitude = (longitude: number) => action(SuggestionsActionTypes.UPDATE_LONGITUDE, longitude);
export const updateUseCoords = (useCoords: boolean) => action(SuggestionsActionTypes.UPDATE_USE_COORDS, useCoords);
export const updateExtended = (extended: boolean) => action(SuggestionsActionTypes.UPDATE_EXTENDED, extended);


export function fetchSuggestion(state: SuggestionsStore) {
    return (dispatch: any) => {
        const query = state.query;
        let url = `https://lb-coveo-suggestion-service-721765774.us-east-2.elb.amazonaws.com/suggestions?q=${query}`;
        if(state.useCoords){
            url += `&latitude=${state.latitude}&longitude=${state.longitude}`;
        }
        if(state.extended){
            url += "&extended"
        }

        return fetch(url)
            .then((response: Response) => response.json())
            .then((json: string) => dispatch(fetchSuggestionsResponse(query, json)))
            .catch((error: any) => {
                console.log(error);
            })
    }
}