
export interface Suggestion{
    readonly name: string;
    readonly latitude: number;
    readonly longitude: number;
    readonly score: number;
    readonly distance: Distance;
}

export interface Distance{
    readonly km: number;
    readonly mi: number;
}

export const initialState: SuggestionsStore = {
    suggestions: [],
    query: "",
    latitude: 0,
    longitude: 0,
    useCoords: false,
    extended: false,
    queryTime: undefined
};

export interface SuggestionsStore{
    readonly suggestions: Suggestion[];
    readonly query: string;
    readonly latitude: number;
    readonly longitude: number;
    readonly useCoords: boolean;
    readonly extended: boolean;
    readonly queryTime: number | undefined;
}

export enum SuggestionsActionTypes {
    FETCH = '@@suggestions/FETCH',
    FETCH_RESPONSE = '@@suggestions/FETCH_RESPONSE',
    UPDATE_QUERY = '@@suggestions/UPDATE_QUERY',
    UPDATE_LATITUDE = '@@suggestions/UPDATE_LATITUDE',
    UPDATE_LONGITUDE = '@@suggestions/UPDATE_LONGITUDE',
    UPDATE_USE_COORDS = '@@suggestions/UPDATE_USE_COORDS',
    UPDATE_EXTENDED = '@@suggestions/UPDATE_EXTENDED',
}