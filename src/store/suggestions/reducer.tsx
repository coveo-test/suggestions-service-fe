import {Reducer} from "redux";
import {initialState, SuggestionsActionTypes, SuggestionsStore} from "./types";
import {fetchSuggestion} from "./actions";

const reducer: Reducer<SuggestionsStore> = (state = initialState, action: any): SuggestionsStore => {

    console.log(action);
    switch (action.type) {
        case SuggestionsActionTypes.FETCH: {
            return state;
        }
        case SuggestionsActionTypes.FETCH_RESPONSE:{
            const query = action.payload.query;
            if(state.query !== query)
                return state;

            const response = action.payload.response;
            const suggestions = response.suggestions === undefined ? [] : response.suggestions.slice(0,50);
            const queryTime = response.time === undefined ? undefined: response.time;
            return {...state, suggestions: suggestions, queryTime: queryTime};
        }
        case SuggestionsActionTypes.UPDATE_QUERY:{
            const query = action.payload;
            state = {...state, query: query};
            action.asyncDispatch(fetchSuggestion(state));
            return state;
        }
        case SuggestionsActionTypes.UPDATE_LATITUDE:{
            const latitude = action.payload;
            state = {...state, latitude: latitude};
            if(state.useCoords)
                action.asyncDispatch(fetchSuggestion(state));
            return state;
        }
        case SuggestionsActionTypes.UPDATE_LONGITUDE:{
            const longitude = action.payload;
            state = {...state, longitude: longitude};
            if(state.useCoords)
                action.asyncDispatch(fetchSuggestion(state));
            return state;
        }
        case SuggestionsActionTypes.UPDATE_USE_COORDS:{
            const useCoords = action.payload;
            state = {...state, useCoords: useCoords};
            action.asyncDispatch(fetchSuggestion(state));
            return state;
        }
        case SuggestionsActionTypes.UPDATE_EXTENDED:{
            const extended = action.payload;
            state = {...state, extended: extended};
            action.asyncDispatch(fetchSuggestion(state));
            return state;
        }
    }

    return state;
};

export { reducer as testReducer};