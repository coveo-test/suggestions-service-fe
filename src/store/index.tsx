import {combineReducers} from "redux";
import {testReducer, SuggestionsStore} from "./suggestions";

export interface MainStore{
    suggestionsStore: SuggestionsStore;
}

export const rootReducer = combineReducers<MainStore>({
    suggestionsStore: testReducer
});